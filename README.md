[project]: https://gitlab.com/nhuvoch/nhuvo-logo
[version]: https://img.shields.io/badge/version-1.0.2-blue.svg
[release]: https://img.shields.io/badge/release-3-blue.svg

# NhuVo Logo

[![version 1.0.2][version]][project]
[![release 3][release]][project]

![NhuVo](nhuvo.png)

## Contributing

Please refer to [CONTRIBUTOR](CONTRIBUTOR.md).

## Authors &amp; contributors

[nhunours]: https://gitlab.com/nhunours

-   Nhu-Hoai Robert VO &mdash; Main developer &mdash; [@nhunours][nhunours]

## License &amp; copyright

This is not an open source project. There is no license.

&copy;2018 Nhu-Hoai Robert VO
