/**
 * @file Resize logo for optimal usage
 * @version 1.0.2
 * @copyright (c)2018 Nhu-Hoai Robert VO
 * @author Nhu-Hoai Robert VO <nhuhoai.vo@nhuvo.ch>
 * @requires fs
 * @requires isSvg
 * @requires libxmljs
 * @since 1.0.2
 */

import fs from 'fs';
import isSvg from 'is-svg';
import libxmljs from 'libxmljs';

fs.readFile('nhuvo.svg', 'utf8', function(err, data) {
  if (err) {
    // Cannot read the file or file not exists
    console.log('nhuvo.svg is not found.');
    process.exit(1);
  } else {
    try {
      const logo = libxmljs.parseXml(data);
      // is it an xml file? check parser errors
      if (logo.errors.length === 0) {
        // No xml error, has an svg standard definition?
        if (isSvg(data)) {
          console.log('nhuvo.svg is an svg file');
          process.exit();
        } else {
          console.log('nhuvo.svg is not an SVG structured file.');
          process.exit(1);
        }
      } else {
        let i;
        for (i = 0; i < logo.errors.length; i++) {
          console.log(logo.errors[i]);
        }
        process.exit(1);
      }
    } catch (e) {
      console.log('nhuvo.svg is not an xml compatible file.');
      process.exit(1);
    }
  }
});
