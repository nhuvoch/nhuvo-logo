/**
 * @file Resize logo for optimal usage
 * @version 1.0.2
 * @copyright (c)2018 Nhu-Hoai Robert VO
 * @author Nhu-Hoai Robert VO <nhuhoai.vo@nhuvo.ch>
 * @requires fs
 * @requires resize-img
 * @since 1.0.2
 */

import fs from 'fs';
import resizeImg from 'resize-img';

const logo = fs.readFileSync('nhuvo.png');

const sizes = [
  1024,
  512,
  256,
  128,
];

sizes.forEach(function(size) {
  resizeImg(logo, {
    width: size,
    height: size,
  }).then(
    (buf) => {
      fs.writeFileSync('build/nhuvo_' + size + '.png', buf);
    }
  );
});
