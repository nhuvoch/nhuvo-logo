# Changelog

[v1.0.2]: https://img.shields.io/badge/version-1.0.2-blue.svg
[r3]: https://img.shields.io/badge/release-3-blue.svg

![version 1.0.2][v1.0.2]
![release 3][r3]

_Test svg file and create resized logo._

<hr />

[v1.0.1]: https://img.shields.io/badge/version-1.0.1-blue.svg
[r2]: https://img.shields.io/badge/release-2-blue.svg

![version 1.0.1][v1.0.1]
![release 2][r2]

_Rotate 45° anticlockwise._

<hr />

[v1.0.0]: https://img.shields.io/badge/version-1.0.0-blue.svg
[r1]: https://img.shields.io/badge/release-1-blue.svg

![version 1.0.0][v1.0.0]
![release 1][r1]

_Create a logo with N &amp; V letters for **N**hu**V**o based on two colors: orange &amp; blue._
